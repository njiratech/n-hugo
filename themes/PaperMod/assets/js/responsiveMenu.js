var forEach = function forEach(nodeList, callback, scope) {
  for (var index = 0; index < nodeList.length; index++) {
    callback.call(scope, nodeList[index], index);
  }
};

var toggleMenu = function toggleMenu() {
  var menuToggle = document.getElementById("menu-toggle");
  var menu = document.getElementById("menu"); // if it's open, close it; if it's closed, open it

  if (menu.classList.contains("open")) {
    menu.classList.remove("open");
    menuToggle.setAttribute("aria-expanded", false);
  } else {
    menu.classList.add("open");
    menuToggle.setAttribute("aria-expanded", true);
  }
};

var getDropdownToggle = function getDropdownToggle(dropdown) {
  return dropdown.querySelector(".dropdown-toggle");
};

var openDropdown = function openDropdown(dropdown) {
  var dropdownToggle = getDropdownToggle(dropdown); // there is no sibling selector, so we jump to the parent and do a query

  var openSiblingDropdowns =
    dropdown.parentNode.querySelectorAll(".dropdown.open"); // first we'll close the other open dropdowns

  forEach(openSiblingDropdowns, function (dropdown) {
    return closeDropdown(dropdown, false);
  }); // finally, set aria-expanded to true and add the open class

  dropdownToggle.setAttribute("aria-expanded", true);
  dropdown.classList.add("open");
}; // close the dropdown

var closeDropdown = function closeDropdown(dropdown) {
  var moveFocus =
    arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var dropdownToggle = getDropdownToggle(dropdown); // set aria-expanded to false and remove the open class

  dropdownToggle.setAttribute("aria-expanded", false);
  dropdown.classList.remove("open"); // move the focus back to the toggle if we're allowed to

  if (moveFocus) {
    dropdownToggle.focus();
  }
}; // close it if it's open or open it if it's closed

var toggleDropdown = function toggleDropdown(event) {
  var dropdown = this.parentNode;
  event.stopPropagation();

  if (dropdown.classList.contains("open")) {
    closeDropdown(dropdown);
  } else {
    openDropdown(dropdown);
  }
}; // see ya later dropdowns

var escapeDropdowns = function escapeDropdowns(event) {
  // if the user clicked anywhere or hit the escape key
  if (event.type === "click" || ("key" in event && event.key === "Escape")) {
    var openDropdowns = document.querySelectorAll(".dropdown.open"); // close all the dropdowns (but don't move focus if the event was a click)

    forEach(openDropdowns, function (dropdown) {
      return closeDropdown(dropdown, event.type !== "click");
    });
  }
}; // hello world

var readySetNavigate = function readySetNavigate() {
  var menuToggle = document.getElementById("menu-toggle");
  var dropdownToggles = document.querySelectorAll(".dropdown-toggle"); // add the click listener for the main menu toggle

  menuToggle.addEventListener("click", toggleMenu); // add the click listener for all of the dropdown toggles

  forEach(dropdownToggles, function (dropdownToggle) {
    dropdownToggle.addEventListener("click", toggleDropdown);
  }); // add the escape listener to close the dropdowns

  document.addEventListener("keyup", escapeDropdowns); // add the click listener to close the dropdowns

  document.addEventListener("click", escapeDropdowns);
}; // fire up

readySetNavigate();
