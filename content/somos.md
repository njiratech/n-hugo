---
title: "Somos"
ShowBreadCrumbs: true
ShowToc: true
---

## SOBRE
---

Njira é um serviço de hospedagem, em pequena escala, dedicado a  iniciativas culturais e políticas. Abrigamos projetos focados na produção de conteúdo cultural, artístico e de transformação social (políticos, comunitários, identitários) na web brasileira.

Observamos que a produção cultural crítica está cada vez menos presente na web por conta da transformação de interesses e visões na internet. É por isso que percebemos a importância de criar modelos organizacionais e de abrigamento para que grupos e iniciativas independentes (websites, arquivos digitais, manutenção de projetos online, etc.) se mantenham ativos com amparo, tutoria, resguardo e proteção.

Acreditamos na liberdade de expressão, e para isso queremos garantir um espaço aberto que respeite a dignidade, aprendizagem e expansão da consciência humana.  Oferecemos gerenciamento compartilhado, com manutenção e estabilidade para serviços de hospedagem web. E privilegiamos o conhecimento aberto e seguro, na tentativa de desmistificar conhecimento técnico por meio do uso de ferramentas livres, e facilitando o contatos e cooperações entre pessoas, colectivos, projetos e organizações, em ambiente digital.



## COMO ABRIGAMOS
---

Desde de 2018 estamos desenvolvendo uma infraestrutura web para acomodar websites, assim como outros arquivos digitais. Realizamos uma manutenção regular, e fazemos o acompanhamento caso a caso para facilitar trocas seguras de informações e conhecimentos entre as pessoas.

Nossa missão é garantir serviços com infraestrutura de internet segura, e suporte técnico voltado às necessidades da comunidade, explorando outros modelos de gestão e convivência em torno de recursos comuns tecnológicos, na internet.



## NOSSO SUPORTE
---

Estamos acompanhando caso a caso e mantemos um grupo para comunicação com xs administradorxs de sites. Se você administra sites hospedados com gente, use o nosso grupo no `Telegram @njira2021` para notificar e resolver qualquer problema técnico. Caso você não use Telegram, envie sua mensagem para: <suporte@njira.tech>



## Apoie a gente!

> Se você tiver interesse em fazer parte da vizinhança da njira por favor leia antes o nosso FAQ. Se identificar com a causa e quiser nos relatar seu caso, escreva para contato@njira.tech
