---
title: "Perguntas Frequentes"
draft: false
ShowBreadCrumbs: false
showtoc: true
tocopen: true
---


### NOSSA HISTÓRIA
---
Enquanto estavamos concebendo o projeto Arquivos Táticos, paralelamente havia uma estrutura virtual sendo planejada e  montada para abrigar o site do projeto, assim como websites e conteúdos digitais de iniciativas e acervos do qual concebemos e colaboramos ao longo de duas décadas de internet brasileira.

Arquivos Táticos é uma cartografia visual e biblioteca, baseada na seleção de sites, documentos e publicações disponíveis em midiatatica.info, editado por Tatiana Wells, e a plataforma desarquivo.org, pesquisa arquivo de Cristina Ribas sobre práticas artísticas no Brasil. Dessa forma, juntamos nossos projetos, iniciamos uma pesquisa feminista-colaborativa, e selecionamos um acervo para referenciar e dar visibilidade à produção de contracultura brasileira na internet.  

É dessa convergência que nasce a Njira, a VPS[^1] para onde migramos esses projetos, e onde foi criada uma infraestrutura mínima para abrigar mais iniciativas que se relacionam. Njira é uma palavra de origem africana que aponta para as possibilidades que se abrem a partir da encruzilhada. E compartilhar da mesma estrutura tecnológica - que significa recursos técnicos, custos, manutenção e segurança - foi um caminho que encontramos para aprender sobre autonomia e cuidado com os arquivos digitais que resguardamos.

Observando o cenário político brasileiro fora das redes sociais com uma produção radical cada vez menos presente na internet, visionamos um futuro que garanta espaço e serviços para comunidades sensíveis dentro deste ecossistema,  abrindo caminhos para manter seus quilombos ativos em espaço virtual.

[^1]:https://pt.wikipedia.org/wiki/Servidor_virtual_privado


<br/>
<br/>

### COMO ABRIGAMOS  
***
Desde de 2018 estamos desenvolvendo uma infraestrutura web para acomodar websites, assim como outros arquivos digitais. Realizamos uma manutenção regular, e fazemos o acompanhamento caso a caso para facilitar trocas seguras de informações e conhecimentos entre as pessoas.

Nossa missão é garantir serviços com infraestrutura de internet segura, e suporte técnico voltado às necessidades da comunidade, explorando outros modelos de gestão e convivência em torno de recursos comuns tecnológicos, na internet.

<br/>
<br/>

### O QUE OFERECEMOS
---
_Hospedagem de site_  

 Fazemos acompanhamento individual na migração, na instalação e configurações gerais de banco de dados, apontamentos de domínio e testes, muitos testes.  Suportamos sites desenvolvidos em Wordpress, e sites estáticos, e aceitamos outras tecnologias também, dependendo da sua paciência e interesse em aprender a instalar e configurar com a gente. Não trabalhamos com uma interface visual de hospedagem, na maior parte das vezes nossa comunicação com a servidora é via terminal, tela preta. Não administramos seu domínio e serviços de e-mail, mas podemos sugerir serviços e opções livres menos custosas e menos vulneráveis.  

 <br/>
 <br/>

 ### NOSSO SUPORTE
 ---

 Estamos acompanhando caso a caso e mantemos um grupo para comunicação com xs administradorxs de sites. Se você administra sites hospedados com gente, use o nosso grupo no [Telegram](https://t.me/joinchat/EafBNozHg8U5YT1W) para notificar e resolver qualquer problema técnico. Caso você não use Telegram, envie sua mensagem para: <suporte@njira.tech>

 <br/>
 <br/>

### NOVXS PARTICIPANTES/USUARIXS
---
_Estou interessadx. Como faço para entrar?_  

Se de alguma forma seu trabalho se identifica com a nossa história, se relaciona com a vizinhança e acredita que sua iniciativa compartilha conhecimento cultural, político e crítico, por favor, relate seu caso pra gente. Nossa única forma de se conhecer ainda é por e-mail no endereço contato@njira.tech. Mesmo que seu site não seja abrigado com a gente, podemos ajudar você  a pensar e pesquisar soluções para o seu caso. Nossa intenção é abrigar aprendendo a cuidar junto.



### CUSTO
---
_Qual é o valor do serviço?_  

A despesa ideal anual de manutenção da Njira é de USD $6,000 , o equivalente a aproximadamente R$ 30334,79[^2] com capacidade para abrigar em torno de 50 iniciativas web. Por enquanto estamos mantendo essa infraestrutura de despesa mínima, num futuro próximo, gostaríamos de aumentar a capacidade de atenção, manutenção e segurança. Para isso, o ideal seria operar com valores um pouco mais altos. Estes custos estão divididos em:  

| Despesas | mensal USD| mensal R$ | anual USD | anual R$ |
| ------ | ------ | ------ | ------ | ------ |
| VPS | 18 | 91 | 216 | 1.092,05 |
| Manutenção técnica | 241 | 1.218,45 | 2,892 | 14.621,37 |
| Coordenação e comunicação | 241 | 1.218,45 | 2,892 | 14.621,37 |
| TOTAL|  |  | 6,000 | 30.334,79 |

- Considerando que, no esquema atual se conseguimos abrigar torno de 50 iniciativas web, o valor equivalente para cada projeto hospedado seria de aproximadamente:
  - `Custo anual para cada site abrigado: R$ 606,70  / USD $120.00`
  - `Custo mensal para cada site abrigado: R$ 50,50 / USD 10.00`

[^2]:A conversão de valores está baseada na cotação do dolar em junho/2021.  
