---
title: "Njira é um serviço de hospedagem,"
ShowBreadCrumbs: false
showtoc: false
tocopen: false

---

em pequena escala, dedicado a  iniciativas culturais e políticas. Abrigamos projetos focados na produção de conteúdo cultural, artístico e de transformação social (políticos, comunitários, identitários) na web brasileira.

Observamos que a produção cultural crítica está cada vez menos presente na web por conta da transformação de interesses e visões na internet. É por isso que percebemos a importância de criar modelos organizacionais e de abrigamento para que grupos e iniciativas independentes (websites, arquivos digitais, manutenção de projetos online, etc.) se mantenham ativos com amparo, tutoria, resguardo e proteção.

Acreditamos na liberdade de expressão, e para isso queremos garantir um espaço aberto que respeite a dignidade, aprendendo e exercitando a consciência de uso.  Oferecemos gerenciamento compartilhado, com manutenção e estabilidade para serviços de hospedagem web. E privilegiamos o conhecimento aberto e seguro, na tentativa de desmistificar conhecimento técnico facilitando o contatos e cooperações entre pessoas, colectivos, projetos e organizações, em ambiente digital.
