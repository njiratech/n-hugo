---
title: "\U00002661  Como posso apoiar?"
ShowBreadCrumbs: false
showtoc: false
tocopen: false

---


Para projetos que preveem apoio financeiro futuro ou está aplicando para algum fundo, pedimos para incluir o custo hospedagem e manutenção na sua rubrica/planilha orçamentária, baseado na estrutura planejada do site. Se você tem interesse em doar horas a partir de tarefas, tais como revisão de conteúdo, tutoriais, oficinas e testes, mande-nos um email para <contato@njira.tech>
