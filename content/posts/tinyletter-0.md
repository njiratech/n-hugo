---
title: "panfleto - MAIO21"
date: 2021-07-29T16:43:57-04:00
draft: false
description: ""
showToc: false
TocOpen: false
ShowBreadCrumbs: true
ShowPostNavLinks: true

---

Oi vizinhança,
Sejam bem-vindxs para nossa guarida :)     

Desde a virada do ano até a chegada do novo ano astrológico, a njira vem nos dando respostas interessantes sobre a experiência ao oferecer estrutura mínima necessária para hospedar sites das redes que participamos e colaboramos.

Só mais recentemente, entendemos a capacidade de espaço que temos para abrigar, qual o suporte técnico necessário para configurar esse espaço, e ainda estamos aprendendo formas de nos comunicar sobre mudanças e problemas técnicos que encontramos no caminho, na construção de uma vizinhança.

* A vizinhança cresceu, de Norte a Sul :)
Ano passado, migramos os sites https://azuis.net/,  https://inesnin.net, https://imotiro.org / e http://munduruku.org. E de lá pra cá, abrigamos novas iniciativas do Pará como https://www.labampe.org/,  http://aninga.org , http://coisasdenegro.com, e em Porto Alegre com o https://mapadeguapuruvus.art/ e https://epistemologiasfeministas.net/  

* Equipe da Coolab junto com a njira
Temos dois sysadmins colaborando atualmente na njira que estão com a gente desde setembro pandêmico XXI. O Iago vem estudando e configurando toda estrutura da vizinhança, e a Tânia que está acompanhando o projeto,  dando gás e reforço emergencial pra gente. Elxs fazem parte do  `https://www.coolab.org/`  iniciativa que agrega diversas pessoas e projetos de comunicação comunitária fomentando infraestruturas autônomas, através da capacitação técnica e ativação.

* Canais de atendimento e suporte  
Por conta da funcionalidade do e-mail para arquivar mensagens e voltar nelas quando necessário, daqui para a frente, vamos centralizar o atendimento de todas as dúvidas e questões técnicas referente à hospedagem do seu site na njira através do `e-mail suporte@njira.tech`. E para quem estiver desenvolvendo diretamente o seu site, temos um grupo no `Telegram @njira2021` específico para administradores de site e sysadmins.

* Um pedido para todes:  precisamos de uma descrição breve do seu site.
Pode ser uma ou duas linhas só. Essa informação será utilizada no nosso boletim mas também será publicada no diretório de sites abrigados pela njira na nossa página web. Então por favor, coloque a descrição da sua iniciativa diretamente no md (Markdown, linguagem de marcação): https://md.cloud.coolab.org/D9y5NBCVQMiGHE22H4xIWA?both , o quanto antes melhor!

Agradecemos quem assinou o Tinyletter para manter a mesma informação chegando para todas as pessoas. Por enquanto é isso.
Muita saúde, axé e longa vida para nós!
