---
title: "Serviços"
ShowBreadCrumbs: false
showtoc: true
tocopen: true

---







### HOSPEDAGEM DE SITE
---


### ETHERPAD
---
[https://pad.njira.tech/](https://pad.njira.tech/)  

Editor de texto simples na web baseado em software livre, bem parecido a um bloco de notas online que serve para criar e editar conteúdos de forma colaborativa. Pads são muito utilizados para criar chamadas, organizar ações, escrever um texto em forma individual ou coletiva, como agenda, tomar notas ou fazer relatorias de reuniões e encontros, entre muitas outras possibilidade. Este tipo de editor permite a contribuição de várias pessoas que podem editar ao mesmo tempo, de forma anônima e sem limite de extensão de texto.

Para criar um bloco de notas online, acesse o site [https://pad.njira.tech/](https://pad.njira.tech/), e lá você tem a opção de criar um pad com um link aleatório, ou se preferir, escreva nome desejado na caixa de texto. Você pode criar quantos pads quiser e qualquer pessoa com o link pode acessar o conteúdo.

E lembre-se:

- guarde bem o link do seu pad, pois não há como procura-lo uma lista de pads para consulta;

- o documento não é privado, mas também não fará parte dos motores de busca como Google, etc. Então, se você estiver trabalhando com conteúdo sensível e que precisa de muita segurança nas suas informações, é melhor utilizar outras opções;

- os pads na njira são deletados após 90 dias para não sobrecarregar a servidora. Se você precisa guardar as informações importantes, por favor, faça um backup. É possível fazer download do conteúdo do pad nos formatos do etherpad, como html ou como editor de texto .txt (esse você vai poder abrir no Word, LibreOffice ou editor de texto que usar no teu computador)
